import rendezvous_app from '../../components/component/rendezvous/rendezvous.vue';
import felicitation from '../../components/component/rendezvous/felicitation.vue';
import confirmRdv from '../../components/component/rendezvous/confirmRdv';



import getRoute from './utils'

let route_rendezvous = getRoute('/rendezvous',rendezvous_app,'rendevous').concat(getRoute('/felicitation',felicitation,'felicitation'),getRoute('/confirm',confirmRdv,'confirm'))




export  default route_rendezvous
